## Details
I wanted to save names and authors of all books that we read to our daughter.
Keeping a record of it manually was too hard and more than that, we missed it completely.

One day,while I was checking my library account , I found an option to save "already read list" and it saved information about books that we borrowed. 

The data from the library had many other infromation. All I had to do was to parse that data and extract information, that I was looking for.This is how, this program was born.

It parses the database which has been downloaded from my library account and then extracts Book Title and Author Names.

Now, I don't need to worry about noting books names and neither do it manually.

Note: The included database "sampledata.txt" has been downloaded from my library account.

## Usage
```
python readbooks.py

Result: 
I :  Reynolds, Peter H
I am here :  Reynolds, Peter H
Beautiful hands :  Otoshi, Kathryn
After the fall :  Santat, Dan
```


## Author
Binaya Joshi
