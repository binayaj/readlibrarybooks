#!/usr/bin/env python
# Author: Binaya Joshi
# Simple code to extract names and authors of books that have been read
# Database is from library account

import re
pattern1=re.compile(r'(TITLE)([A-Za-z\s\\]*)')
pattern2=re.compile(r'(AUTHOR)([A-Za-z,\s\\]*)')
book_name=""
author=""

with open ("sampledata.txt",'r') as fp:
      for line in fp:
  	author_info=pattern2.findall(line)
  	book_info=pattern1.findall(line)
	if len(author_info)>0:
             temp= (author_info[0][1].strip().split(","))
             author=temp[0]+","+temp[1] 
               
	if len(book_info)>0:
             m=str (book_info[0])
             book_name=m.split(",")[1][2:-2].strip()
             print (book_name+" :  "+author)
           
		
